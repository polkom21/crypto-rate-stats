import {BootMixin} from '@loopback/boot';
import {ApplicationConfig} from '@loopback/core';
import {CronComponent} from '@loopback/cron';
import {RepositoryMixin, SchemaMigrationOptions} from '@loopback/repository';
import {RestApplication} from '@loopback/rest';
import {
  RestExplorerBindings,
  RestExplorerComponent,
} from '@loopback/rest-explorer';
import {ServiceMixin} from '@loopback/service-proxy';
import path from 'path';
import {Pair} from './models';
import {PairRepository} from './repositories';
import {MySequence} from './sequence';

export {ApplicationConfig};

export class CryptoRateStatsApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication)),
) {
  constructor(options: ApplicationConfig = {}) {
    super(options);

    // Set up the custom sequence
    this.sequence(MySequence);

    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    // Customize @loopback/rest-explorer configuration here
    this.configure(RestExplorerBindings.COMPONENT).to({
      path: '/explorer',
    });
    this.component(RestExplorerComponent);

    this.component(CronComponent);

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };
  }

  async migrateSchema(options?: SchemaMigrationOptions) {
    await super.migrateSchema(options);

    const pairRepo = await this.getRepository(PairRepository);

    const pairs: Partial<Pair>[] = [
      {baseCurrency: 'BTC', targetCurrency: 'USD'},
      {baseCurrency: 'BTC', targetCurrency: 'EUR'},
      {baseCurrency: 'ETH', targetCurrency: 'USD'},
      {baseCurrency: 'ETH', targetCurrency: 'EUR'},
    ];

    await pairRepo.createAll(pairs);
  }
}
