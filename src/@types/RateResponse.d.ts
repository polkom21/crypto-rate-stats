export interface RateResponse {
  [key: string]: {[key: string]: number};
}
