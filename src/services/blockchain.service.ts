import {/* inject, */ BindingScope, injectable} from '@loopback/core';
import axios from 'axios';
import {RateResponse} from '../@types/RateResponse';

@injectable({scope: BindingScope.TRANSIENT})
export class BlockchainService {
  constructor(/* Add @inject to inject parameters */) {}

  async getRates(
    baseCurrency: string,
    targetCurrency: string,
    timestamp: string,
  ): Promise<RateResponse> {
    const response = await axios.get(
      `https://www.blockchain.com/prices/api/coin-historical-price?fsym=${baseCurrency}&tsyms=${targetCurrency}&ts=${timestamp}&limit=1`,
    );
    return response.data as RateResponse;
  }
}
