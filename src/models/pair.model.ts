import {Entity, model, property} from '@loopback/repository';

@model()
export class Pair extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  baseCurrency: string;

  @property({
    type: 'string',
    required: true,
  })
  targetCurrency: string;

  constructor(data?: Partial<Pair>) {
    super(data);
  }
}

export interface PairRelations {
  // describe navigational properties here
}

export type PairWithRelations = Pair & PairRelations;
