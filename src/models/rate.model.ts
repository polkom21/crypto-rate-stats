import {Entity, model, property} from '@loopback/repository';

@model()
export class Rate extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  baseCurrency: string;

  @property({
    type: 'string',
    required: true,
  })
  targetCurrency: string;

  @property({
    type: 'date',
    required: true,
  })
  date: string;

  @property({
    type: 'number',
    required: true,
  })
  rate: number;

  constructor(data?: Partial<Rate>) {
    super(data);
  }
}

export interface RateRelations {
  // describe navigational properties here
}

export type RateWithRelations = Rate & RateRelations;
