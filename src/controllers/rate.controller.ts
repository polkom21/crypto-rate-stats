import {service} from '@loopback/core';
import {CronJob, cronJob} from '@loopback/cron';
import {get, param} from '@loopback/openapi-v3';
import {repository} from '@loopback/repository';
import {HttpErrors} from '@loopback/rest';
import moment from 'moment';
import {Rate} from '../models';
import {PairRepository, RateRepository} from '../repositories';
import {BlockchainService} from '../services';

@cronJob()
export class RateController extends CronJob {
  rateTick = 1; // In minutes

  constructor(
    @repository(PairRepository)
    protected pairRepo: PairRepository,
    @repository(RateRepository)
    protected rateRepo: RateRepository,
    @service(BlockchainService)
    protected blockchainService: BlockchainService,
  ) {
    super({
      name: 'update-rates',
      onTick: () => {
        // Update rates
        this.updateRates().catch(console.error);
      },
      cronTime: '*/1 * * * *', // Every 5 minute
      start: false,
    });
  }

  private async updateRates() {
    const pairs = await this.pairRepo.find();
    const lastRate = await this.rateRepo.findOne({order: ['id DESC']});
    const nowDate = moment().set('second', 0);
    const ticks: string[] = [];

    if (
      lastRate &&
      Math.abs(moment(nowDate).diff(lastRate.date)) > this.rateTick
    ) {
      ticks.push(
        moment(lastRate.date).add(this.rateTick, 'minute').format('X'),
      );
      while (Number(ticks[ticks.length - 1]) < Number(nowDate.format('X'))) {
        ticks.push(
          moment(ticks[ticks.length - 1], 'X')
            .add(this.rateTick, 'minute')
            .format('X'),
        );
      }
    } else {
      ticks.push(nowDate.format('X'));
    }

    const rates = await Promise.all(
      ticks.map(tick =>
        Promise.all(
          pairs.map(pair =>
            this.blockchainService.getRates(
              pair.baseCurrency,
              pair.targetCurrency,
              tick,
            ),
          ),
        ),
      ),
    );
    const ratesToAdd: Partial<Rate>[] = [];

    rates.forEach((pairRates, index) => {
      const date = moment(ticks[index], 'X');
      pairRates.forEach(pairRate => {
        Object.entries(pairRate).forEach(([key, value]) => {
          ratesToAdd.push({
            baseCurrency: key,
            targetCurrency: Object.keys(value)[0],
            date: date.toString(),
            rate: Object.values(value)[0],
          });
        });
      });
    });

    await this.rateRepo.createAll(ratesToAdd);

    return;
  }

  @get('/rate')
  async getRates(
    @param.query.string('baseCurrency') baseCurrency: string,
    @param.query.string('targetCurrency') targetCurrency: string,
    @param.query.string('from', {
      description: 'From date timestamp',
      required: false,
    })
    from?: string,
    @param.query.string('to', {
      description: 'To date timestamp',
      required: false,
    })
    to?: string,
    @param.query.number('granularity', {description: 'In minutes'})
    granularity = 10,
  ) {
    const pair = await this.pairRepo.findOne({
      where: {baseCurrency, targetCurrency},
    });

    if (!pair) {
      throw new HttpErrors.BadRequest(
        'Invalid pair of base currency and target currency',
      );
    }

    let fromDate = from
      ? moment(from, 'X').set('second', 0)
      : moment().subtract(24, 'hours').set('second', 0);
    const toDate = to
      ? moment(to, 'X').set('second', 0)
      : moment().set('second', 0);

    const datesToGet = [];

    while (fromDate.format('X') < toDate.format('X')) {
      datesToGet.push(fromDate.toString());
      fromDate = fromDate.add(granularity, 'minutes');
    }

    const rates = await this.rateRepo.find({
      where: {
        baseCurrency,
        targetCurrency,
        or: datesToGet.map(d => ({date: d})),
      },
    });

    return {
      baseCryptoCurrency: baseCurrency,
      targetCurrency: targetCurrency,
      rates: rates.map(rate => ({
        date: rate.date,
        rate: rate.rate,
      })),
    };
  }
}
