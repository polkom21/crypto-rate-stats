import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Pair, PairRelations} from '../models';

export class PairRepository extends DefaultCrudRepository<
  Pair,
  typeof Pair.prototype.id,
  PairRelations
> {
  constructor(@inject('datasources.db') dataSource: DbDataSource) {
    super(Pair, dataSource);
  }
}
